package codewars.easy;

//8 Kyu
//https://www.codewars.com/kata/5513795bd3fafb56c200049e

public class KataThree{

    public static int[] countBy(int x, int n){

        int y ,z = 0;
        int[] arr = new int[n];
        for(int i = 0; i != n; i++) {
            y = z;
            z = x + y;
            arr[i] = z;
        }
        return arr;
    }
}