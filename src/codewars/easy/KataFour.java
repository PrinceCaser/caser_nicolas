package codewars.easy;

//8 Kyu
//https://www.codewars.com/kata/57eadb7ecd143f4c9c0000a3

public class KataFour {

    public static String abbrevName (String name) {

        name.toUpperCase();
        int x = name.lastIndexOf("");
        return
                name.substring(0, 1).toUpperCase() + "." + name.substring(x + 1,x + 2).toUpperCase();
    }

}
