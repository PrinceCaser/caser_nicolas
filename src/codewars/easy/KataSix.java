package codewars.easy;

//8 Kyu
//https://www.codewars.com/kata/5583090cbe83f4fd8c000051


public class KataSix {
    public static int[] digitize(long n) {

        StringBuilder st = new StringBuilder(String.valueOf(n));
        String newST[] = st.reverse().toString().split("");

        int arrInt [] = new int[newST.length];
        for (int i = 0; i < newST.length; i++) {
            arrInt[i] = Integer.parseInt(newST[i]);

        }
        return arrInt;
    }
}

