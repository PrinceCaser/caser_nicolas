package codewars.easy;

//8 Kyu
//https://www.codewars.com/kata/5672a98bdbdd995fad00000f

public class KataTwo {

    public static String rps(String p1, String p2) {

        if(p1 == p2) {
            return "Draw!";
        } else if (p1 == "scissors") {
            return (p2 =="rock") ? "Player 2 won!" : "Player 1 won!";
        } else if (p1 == "paper") {
            return (p2 =="scissors") ? "Player 2 won!" : "Player 1 won!";
        } else if (p1 == "rock") {
            return (p2 == "paper") ? "Player 2 won!" : "Player 1 won!";
        }
        return null;
    }
}