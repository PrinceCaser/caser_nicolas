package codewars.easy;

//8 Kyu
//https://www.codewars.com/kata/54edbc7200b811e956000556

public class KataFive {

    public int countSheep (Boolean[] arrayOfSheep) {

        int count = 0;
        for (int i = 0; i != arrayOfSheep.length; i++) {
            if (arrayOfSheep[i] != null && arrayOfSheep[i]) {
                    count++;
            }
        }
        return count;
    }
}
