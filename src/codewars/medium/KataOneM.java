package codewars.medium;

//6 Kyu
//https://www.codewars.com/kata/525f50e3b73515a6db000b83

public class KataOneM {

    public static String createPhoneNumber(int[] numbers) {

        String number = ("(xxx) xxx-xxxx");
        for (int i : numbers) {
            number = number.replaceFirst("x", Integer.toString(i));
        }
        return number;
    }
}