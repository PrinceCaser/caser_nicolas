package codewars.medium;

//5 Kyu
//https://www.codewars.com/kata/520b9d2ad5c005041100000f

public class KataFourM {

    public static String pigIt(String str) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0, j = 0; j <= str.length(); j++) {
            char ch = j < str.length() ? str.charAt(j) : '\0';
            if (Character.isLetterOrDigit(ch))
                continue;
            if (i < j) {
                sb.append(str.substring(i + 1, j));
                sb.append(str.charAt(i));
                sb.append("ay");
            }
            if (ch != '\0')
                sb.append(ch);
            i = j + 1;
        }
        return sb.toString();
    }
}
