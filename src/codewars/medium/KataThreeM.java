package codewars.medium;

//6 Kyu
//https://www.codewars.com/kata/517abf86da9663f1d2000003

public class KataThreeM {

        public static String toCamelCase(String s) {

            String camelCaseString;
            StringBuilder sb = new StringBuilder(s);

            camelCaseString = sb.toString().replaceAll("[-_]", "");

            return camelCaseString;
        }
    }