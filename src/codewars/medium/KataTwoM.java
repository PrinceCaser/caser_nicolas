package codewars.medium;

//6 Kyu
//https://www.codewars.com/kata/556deca17c58da83c00002db

public class KataTwoM {

    public double[] tribonacci(double[] s, int n) {

        double[] newArr = new double[n];
        for(int i = 0; i < n; i++) {
            newArr[i] = (i < 3) ? s[i] : newArr[i - 3] + newArr[i - 2] + newArr[i - 1];
        }
        return newArr;
    }
}